var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/node-android1');
//mongoose.connect('mongodb://localhost/company');
var Schema = mongoose.Schema({
	username: {type:String,required:true},
	lastname:{type:String,required:true},
	email:{type:String,unique:true, index: true,required:true,match:/.+@.+\..+/,},
	password:{type:String,required:true},
	cpassword:{type:String,required:true},

});
Schema.index({email:1},{unique:true});
module.exports = mongoose.model('users', Schema);
 